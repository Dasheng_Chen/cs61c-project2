/*
 * CS61C Spring 2014 Project2 Reminders:
 * 
 * DO NOT SHARE CODE IN ANY WAY SHAPE OR FORM, NEITHER IN PUBLIC REPOS OR FOR DEBUGGING.
 * 
 * This is one of the two files that you should be modifying and submitting for this project.
 */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.Math;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class SolveMoves {
  public static class Map extends Mapper<IntWritable, MovesWritable, IntWritable, ByteWritable> {
    int boardWidth;
    int boardHeight;
    boolean OTurn;

    /**
     * Configuration and setup that occurs before map gets called for the first time.
     * 
     **/
    @Override
    public void setup(Context context) {
    }

    /**
     * The map function for the second mapreduce that you should be filling out.
     */
    @Override
    public void map(IntWritable key, MovesWritable val, Context context) throws IOException,
        InterruptedException {
      /* YOUR CODE HERE */
      for (int values : val.getMoves()) {
        context.write(new IntWritable(values), new ByteWritable(val.getValue()));
      }
    }
  }


  // reduce
  public static class Reduce extends Reducer<IntWritable, ByteWritable, IntWritable, MovesWritable> {

    int boardWidth;
    int boardHeight;
    int connectWin;
    boolean OTurn;

    /**
     * Configuration and setup that occurs before map gets called for the first time.
     * 
     **/
    @Override
    public void setup(Context context) {
      // load up the config vars specified in Proj2.java#main()
      boardWidth = context.getConfiguration().getInt("boardWidth", 0);
      boardHeight = context.getConfiguration().getInt("boardHeight", 0);
      connectWin = context.getConfiguration().getInt("connectWin", 0);
      OTurn = context.getConfiguration().getBoolean("OTurn", true);
    }

    private int undoDrop(int col, char[] board) {
      char opposingChip = OTurn == true ? 'O' : 'X';
      int i = boardHeight * col + boardHeight - 1;
      while (i >= boardHeight * col && board[i] == ' ') {
        i--;
      }
      if (i < 0) i = 0;
      if (board[i] == ' ' || board[i] == opposingChip) {
        return 0;
      } else {
        board[i] = ' ';
        return 1;
      }
    }

    /**
     * The reduce function for the first mapreduce that you should be filling out.
     */
    @Override
    public void reduce(IntWritable key, Iterable<ByteWritable> values, Context context)
        throws IOException, InterruptedException {
      /* YOUR CODE HERE */

      boolean hasOptimalMove = false;
      boolean hasDraw = false;
      boolean hasWorstMove = false;
      boolean hasUndecided = false;
      byte byte_value_Optimal = 0;
      byte byte_value_Draw = 0;
      byte byte_value_WorstMove = 0;
      byte byte_value_Undecided = 0;
      int optimal;
      int draw = 3;
      int worst;
      int undecied = 0;
      
      // check who's turn, and setup the win lose status
      if (OTurn == true) {
        // get minimum
        optimal = 1;
        worst = 2;
      } else {
        // get maximum
        optimal = 2;
        worst = 1;
      }
      
      // setup the list for each status
      PriorityQueue<Byte> pqWinList = new PriorityQueue<Byte>();
      ArrayList<Byte> alDrawList = new ArrayList<Byte>();
      ArrayList<Byte> alLoseList = new ArrayList<Byte>();
      boolean itIsValid = false;
      for (ByteWritable v : values) {
        if ((v.get() >> 2) == 0) {
          itIsValid = true;
        }
        if ((v.get() & 3) == optimal) {
          byte_value_Optimal = (byte) (v.get() + 4);
          pqWinList.add((Byte) byte_value_Optimal);
          hasOptimalMove = true;
        } else if ((v.get() & 3) == draw) {
          byte_value_Draw = (byte) (v.get() + 4);
          alDrawList.add((Byte) byte_value_Draw);
          hasDraw = true;
        } else if ((v.get() & 3) == worst) {
          byte_value_WorstMove = (byte) (v.get() + 4);
          alLoseList.add((Byte) byte_value_WorstMove);
          hasWorstMove = true;
        }
      }

      // invalid parents check
      if (itIsValid == false) {
        return;
      }

      // minimax, choose the order from smallest win, bigest draw, bigest lose
      if(!pqWinList.isEmpty()){
        byte_value_Optimal = (byte) pqWinList.peek();
      } else if(!alDrawList.isEmpty()){
        byte max = (byte) 0;
        for(byte s : alDrawList){
          if(s > max){
            max = s;
          }
        }
        byte_value_Draw = max;
      } else{
        byte max = (byte) 0;
        for(byte s : alLoseList){
          if(s > max){
            max = s;
          }
        }
        byte_value_WorstMove = max;
      }
      
      
      String board = Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight);
      int hashedKey;
      ArrayList<Integer> al = new ArrayList<Integer>();
      for (int i = 0; i < boardWidth; i++) {
        char[] boardCharArrayRep = board.toCharArray();
        String tmp;
        if (undoDrop(i, boardCharArrayRep) == 1) {
          tmp = new String(boardCharArrayRep);
          hashedKey = Proj2Util.gameHasher(tmp, boardWidth, boardHeight);
          al.add(hashedKey);
        }
      }

      int[] moves = new int[al.size()];
      for (int i = 0; i < al.size(); i++) {
        moves[i] = al.get(i);
      }

      if (hasOptimalMove) {
        context.write(key, new MovesWritable(byte_value_Optimal, moves));
        return;
      }
      if (hasDraw) {
        context.write(key, new MovesWritable(byte_value_Draw, moves));
        return;
      }
      if (hasWorstMove) {
        context.write(key, new MovesWritable(byte_value_WorstMove, moves));
        return;
      }

    }
  }
}
