/*
 * CS61C Spring 2014 Project2 Reminders:
 * 
 * DO NOT SHARE CODE IN ANY WAY SHAPE OR FORM, NEITHER IN PUBLIC REPOS OR FOR DEBUGGING.
 * 
 * This is one of the two files that you should be modifying and submitting for this project.
 */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.Math;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class PossibleMoves {
  public static class Map extends Mapper<IntWritable, MovesWritable, IntWritable, IntWritable> {
    int boardWidth;
    int boardHeight;
    boolean OTurn;
    int childIndex;

    /**
     * Configuration and setup that occurs before map gets called for the first time.
     * 
     **/
    @Override
    public void setup(Context context) {
      // load up the config vars specified in Proj2.java#main()
      boardWidth = context.getConfiguration().getInt("boardWidth", 0);
      boardHeight = context.getConfiguration().getInt("boardHeight", 0);
      OTurn = context.getConfiguration().getBoolean("OTurn", true);
    }

    /**
     * The map function for the first mapreduce that you should be filling out.
     */
    @Override
    public void map(IntWritable key, MovesWritable val, Context context) throws IOException,
        InterruptedException {
      /* YOU CODE HERE */
      int stat = val.getStatus();
      if (stat != 0) {
        return;
      }

      String currBoardRep = Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight);
      for (int i = 0; i < boardWidth; i++) {
        char[] boardRepCharArray = currBoardRep.toCharArray();
        if (dropChip(i, boardRepCharArray) == 1) {
          int hashedKey =
              Proj2Util.gameHasher(new String(boardRepCharArray), boardWidth, boardHeight);
          context.write(new IntWritable(hashedKey), key);
        }
      }
    }

    /**
     * dropChip drops a chip on a column specified on the parameter
     * 
     * @param col is the nth(starts from zero) in a board you want to drop the chip in
     * @param board is the board you are dropping chip in
     * @return 1 if chip was successfully dropped, otherwise 0
     */
    private int dropChip(int col, char[] board) {
      int i = col * boardHeight;
      while(i < (col + 1) * boardHeight && board[i] != ' '){
        i++;
      }
      if (i < (col + 1) * boardHeight && board[i] == ' ') {
        board[i] = (OTurn == true ? 'O' : 'X');
        return 1;
      }
      return 0;
    }
  }

  public static class Reduce extends Reducer<IntWritable, IntWritable, IntWritable, MovesWritable> {
    
    int boardWidth;
    int boardHeight;
    int connectWin;
    boolean OTurn;
    boolean lastRound;

    /**
     * Configuration and setup that occurs before reduce gets called for the first time.
     * 
     **/
    @Override
    public void setup(Context context) {
      // load up the config vars specified in Proj2.java#main()
      boardWidth = context.getConfiguration().getInt("boardWidth", 0);
      boardHeight = context.getConfiguration().getInt("boardHeight", 0);
      connectWin = context.getConfiguration().getInt("connectWin", 0);
      OTurn = context.getConfiguration().getBoolean("OTurn", true);
      lastRound = context.getConfiguration().getBoolean("lastRound", true);
    }

    /**
     * The reduce function for the first mapreduce that you should be filling out.
     */
    @Override
    public void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
        throws IOException, InterruptedException {
      /* YOU CODE HERE */
      String unHashedKey = Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight);
      boolean hasWinner = Proj2Util.gameFinished(unHashedKey, boardWidth, boardHeight, connectWin);
      ArrayList<Integer> al = new ArrayList<Integer>();
      int numOfMoves = 0;
      for (IntWritable value : values) {
        al.add(new Integer(value.get()));
      }
      int[] arr = new int[al.size()];
      for (int i = 0; i < al.size(); i++) {
        arr[i] = al.get(i);
      }
      MovesWritable mw;
      if (hasWinner == true) {
        if (OTurn == true) {
          mw = new MovesWritable((byte) 1, arr);
        } else {
          mw = new MovesWritable((byte) 2, arr);
        }
      } else { /* draw or undecided */
        if (lastRound == true) {
          mw = new MovesWritable((byte) 3, arr);
        } else {
          mw = new MovesWritable((byte) 0, arr);
        }
      }
      context.write(key, mw);
    }
  }
}
